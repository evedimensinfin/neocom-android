package org.dimensinfin.eveonline.neocom.core.domain;

public enum EExtras {
	CAPSULEER_IDENTIFIER, PLANET_IDENTIFIER, PLANET_TYPE_NAME,
	FIRE_AUTHORIZATION_FLOW, REFINETYPE_IDENTIFIER, RESOURCESYSTEM_IDENTIFIER, ASSETFILTER_DEFINITION
}
