package org.dimensinfin.eveonline.neocom.app.activity.core;

import android.view.View;

public interface INeoComActionBar {
	View getView();
}
