package org.dimensinfin.eveonline.neocom.app.domain;

public interface IDataProvider {
	String getDataFormatted();
	boolean isCompleted();
}
