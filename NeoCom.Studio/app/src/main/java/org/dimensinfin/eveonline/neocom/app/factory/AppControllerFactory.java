package org.dimensinfin.eveonline.neocom.app.factory;

import org.dimensinfin.android.mvc.controller.ControllerFactory;
import org.dimensinfin.android.mvc.controller.IAndroidController;
import org.dimensinfin.core.interfaces.ICollaboration;
import org.dimensinfin.eveonline.neocom.app.controller.AppVersionController;
import org.dimensinfin.eveonline.neocom.app.controller.CredentialController;
import org.dimensinfin.eveonline.neocom.app.controller.InitializationExceptionController;
import org.dimensinfin.eveonline.neocom.app.controller.InitializationTaskController;
import org.dimensinfin.eveonline.neocom.app.controller.NeoComActionBarController;
import org.dimensinfin.eveonline.neocom.app.controller.NewLoginController;
import org.dimensinfin.eveonline.neocom.app.controller.PanelTitleController;
import org.dimensinfin.eveonline.neocom.app.controller.PilotController;
import org.dimensinfin.eveonline.neocom.app.controller.PilotSectionController;
import org.dimensinfin.eveonline.neocom.app.controller.RetryButtonController;
import org.dimensinfin.eveonline.neocom.app.controller.SplashActionBarController;
import org.dimensinfin.eveonline.neocom.app.domain.AppVersion;
import org.dimensinfin.eveonline.neocom.app.domain.InitializationException;
import org.dimensinfin.eveonline.neocom.app.domain.InitializationTask;
import org.dimensinfin.eveonline.neocom.app.domain.NewLoginAction;
import org.dimensinfin.eveonline.neocom.app.domain.PanelTitle;
import org.dimensinfin.eveonline.neocom.app.domain.PilotSection;
import org.dimensinfin.eveonline.neocom.app.domain.RetryButton;
import org.dimensinfin.eveonline.neocom.app.ui.DashboardActionBar;
import org.dimensinfin.eveonline.neocom.app.ui.SplashActionBar;
import org.dimensinfin.eveonline.neocom.database.entities.Credential;
import org.dimensinfin.eveonline.neocom.domain.Pilot;

public class AppControllerFactory extends ControllerFactory {
	public AppControllerFactory( final String selectedVariant ) {
		super(selectedVariant);
	}

	/**
	 * This method returns the matching Part for an input node class. Depending on the class and the variant we can generate a different
	 * set of controllers to build up our model-view-controller structures.
	 *
	 * @param node the application node for what we should find a Part and create an instance.
	 * @return new <code>IPart</code> instance that is able to manage this class of node.
	 */
	@Override
	public IAndroidController createController( final ICollaboration node ) {
		logger.info("-- [AppControllerFactory.createController]> Node class: {}", node.getClass().getSimpleName());
		if (node instanceof AppVersion) {
			return new AppVersionController((AppVersion) node, this).setRenderMode( this.getVariant() );
		}
		if (node instanceof InitializationTask) {
			return new InitializationTaskController((InitializationTask) node, this);
		}
		if (node instanceof InitializationException) {
			return new InitializationExceptionController((InitializationException) node, this);
		}
		if (node instanceof RetryButton) {
			return new RetryButtonController((RetryButton) node, this);
		}


		if (node instanceof Pilot) {
			return new PilotController((Pilot) node, this);
		}
		if (node instanceof Credential) {
			return new CredentialController((Credential) node, this);
		}
		if (node instanceof NewLoginAction) {
			return new NewLoginController((NewLoginAction) node, this);
		}
//		if (node instanceof InitialisationTask) {
//			return new InitialisationTaskController((InitialisationTask) node, this);
//		}
		if (node instanceof PanelTitle) {
			return new PanelTitleController.Builder((PanelTitle) node, this)
					       .withVariant(this.getVariant())
					       .build();
		}

//		if (node instanceof PilotV3) {
//			return new PilotV3Controller.Builder((PilotV3) node, this)
//					       .withVariant(this.getVariant())
//					       .build();
//		}
//		if (node instanceof NeoComMenuBar) {
//			return new NeoComDashboardMenuController.Builder((NeoComMenuBar) node, this)
//					.withVariant(this.getVariant())
//					.build();
//		}
		if (node instanceof PilotSection) {
			return new PilotSectionController((PilotSection) node, this);
		}

		// - A C T I O N B A R S
		if (node instanceof SplashActionBar) {
			return new SplashActionBarController((SplashActionBar) node, this);
		}
		if (node instanceof DashboardActionBar) {
			return new NeoComActionBarController((DashboardActionBar) node, this);
		}
//		if (node instanceof PlanetaryActionBar) {
//			return new AssetActionBarController((PlanetaryActionBar) node, this);
//		}
//		if (node instanceof MiningActionBar) {
//			return new MiningActionBarController((MiningActionBar) node, this);
//		}
		return super.createController(node);
	}
}
