package org.dimensinfin.eveonline.neocom.test.support.validators;

public interface IValidator {
	boolean validate();
}
