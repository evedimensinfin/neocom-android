package org.dimensinfin.eveonline.neocom.test.support.ristretto;

import android.view.View;

@FunctionalInterface
public interface ViewCheck {
	boolean check( final View view );
}
