package org.dimensinfin.eveonline.neocom.visual.app.support;

public interface IConverter<F, T> {
	T convert( F input );
}
